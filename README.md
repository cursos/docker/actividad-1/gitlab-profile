# Propuesta

Se propone realizar la configuración de un stack (`docker-compose.yml`) de **Wordpress** con todo lo necesario para su correcta ejecución. 

- Crear un repo por persona o grupo de hasta 3
    - nombre: `<primera-letra-del-nombre><apellido>` `<primera-letra-del-nombre><apellido>`. Ej: vcanete mluna
    - ruta: `<primera-letra-del-nombre><apellido>-<primera-letra-del-nombre><apellido>`. Ej: vcanete-mluna
    - Puede ser privado
- El repo debe contener el stack **funcionando**, es decir, el `docker-compose.yml` previamente probado en local y un `README.md` con la descripción de la configuración que aplicaron, y en caso de no usar imágenes oficiales, justificar. 


